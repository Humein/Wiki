**This (and all other documents) should be changed and updated with discussion from the community. All is subject to change, start a discussion on what you think should be changed**
<br>
<br>

**Goal**: <mark>Create a self governing, safe, and diverse online community that fosters discussion over both original content and shared links.</mark>

<br>

**Execution**:

Before delving deeper into our primary objective, let me first provide some context about how I define "community" in this context. Imagine a thriving farm as an analogy for an effective community. In such a farm, decisions regarding crop selection, planting, harvesting, and pest control are made by the people who use the land and eat the crops - the farmers themselves. Large corporations do not dictate which crops are grown or when to sell them; rather, the farmers cultivate what they believe is tasteful, practical, and enjoyable. Similarly, undesirable growths are removed from the soil, but non-invasive unintended vegetation is tolerated if they pose no detrimental impact on other species.

A healthy community functions similarly. Its residents participate actively in shaping its destiny through mutual decision-making, conflict resolution, and shared responsibility. Even though individuals might disagree on certain matters, they collaborate to reach agreements and accommodate diverse perspectives to ensure overall satisfaction for everyone concerned. In essence, the key difference between a realm managed by external entities versus one self-governed lies in who wields power over planning and implementation processes and whether there exists scope for individual preferences within the framework of communal values.

<br><br>

With that out of the way I'm now going to dive into my vision for the rest of the community. However, these are just my ideas. As explained a good community must come to descsions themselves without one person dictating too much.

Okay. I’ve previously mentioned self-government where decisions are made by the community. Now, let me delve deeper into the specifics. While others might have varying perspectives on this matter, here are my thoughts regarding how our growing community can be governed effectively:

Two methods we can use for community administration are consensus voting and sortition.

For our community, consensus voting could address various topics, including moderation policies, technical improvements, content development, admission criteria, objectives, finances, dispute resolution, etc. Everyone has a say through their participation. To prevent misuse, requirements like minimal involvement duration might be implemented. Attempting full agreement is ideal, but if unattainable, a small margin of error could be accepted. Regular “meta” discussions enable members to voice concerns and find solutions.

Moderation presents a challenge. Unacceptable material shouldn’t remain accessible without intervention. With that said, traditional election or appointment by admin systems aren’t great either. Instead of relying solely on voting, we could practice sortition – selecting individuals at regular intervals (e.g., two months to avoid supermos) via a random process to oversee the community. These randomly chosen moderators would follow predetermined regulations decided upon by the whole group. The new team prepares its successors before rotating out of service. Any disputes are settled by seeking consensus from the entire community.

These suggestions serve only as a starting point for discussion.

Creating a online community is one of the most difficult tasks. There are several ways to approach this challenge. Firstly, all rules should be formulated by the community itself through discussions. Some options for creating these guidelines include implementing an overarching rusafele such as  beehaw's "be(e) nice," establishing a detailed set of regulations that are harder to circumvent, or finding a balance between the two approaches. Furthermore, apart from moderators enforcing these rules, the community must foster a culture of kindness where unacceptable behavior is discouraged. New members should be made aware of this policy upon joining. One more method to prevent the presence of haters and trolls is requiring membership applications, though this idea requires further deliberation due to its potential impact on growth. Defederating with toxic instances and disabling upvotes should also be thought about. Overall, building a safe virtual space necessitates collaboration and thoughtful planning.


When it comes to the content discussed on the platform, the specifics should ultimately be determined by the users themselves. Nonetheless, promoting varied conversation topics seems desirable. Content filtering may legal challenges when attempting to manage adult material, that should probably not be allowed. To encourage individuals to contribute without reservations, there needs to be assurances that their posts won't receive unfair downvotes solely based on controversy or originality. Additionally, deciding whether user-generated communities should be established freely or regulated by administrators warrants consideration.


Well, I think that is (currently) the end of my explanation. Now we need to actually build the site. If you are at all moved by this vision please consider helping out by joining. You don't need any skills at all just the ability to participate in a community. There will be a lot that needs to be decided as the community is started (with one of most minor being the name) but once it's going I believe it will be quite simple to manage.


Please note that the most important part of this community is the fact that it's self governed. Therefore, it should be noted that all ideas here are mine and are not final.


Written by Henry 6/2023
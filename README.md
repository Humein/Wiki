# Wiki

Wiki/Docs for Humein. Contains information regarding the project, explanation of community culture, tech issues, etc.

Currently I'm not using the docs built into codeberg and am instead using the repo itself with the goal of eventually moving everything to a static site.

## Pages
[Vision](https://codeberg.org/Humein/Wiki/src/branch/main/vision-statement.md): Contains an explanation of what/how we're trying to do. This is the foundation that everything is built upon.

[Inclusitivity Statement](https://codeberg.org/Humein/Wiki/src/branch/main/inclusitivity-statement.md):Contains a list of our values, specifcially some promoting inclusitivity.
﻿# Humein: Fostering a community

One of the biggest questions we need to solve is how to foster a community allows for conflicting viewpoints to coexist and debate each other in good faith. If this community is properly realized, then moderation will be needed minimally as people understand how to moderate themselves.

  
## The Three Posters
In most online debates (especially around politics) there are three main types of posters, The Troll, The Passionist, and The Normie. Below I’ve outlined each group, how to tell who’s who and how to try to keep the toxic groups under control. You (as a normie) should read through each to inform yourself of the folly of online groups.

  

### The Troll

  

The troll is only here for this debate to get a rise out of people. They don’t really care about the argument they’re pushing, they’re only doing it to make people mad, shocked, upset and angry. Troll’s relish in your harm. They may or may not believe in their stance; all they care about is getting a rise out of you. Often they will bait other, more sensible, people into blowing up against the troll which often looks something like this:

  

“Troll: Pineapple on pizza is disgusting and anyone who thinks otherwise can go die in a hole.

  

Normal person: How dare you say pineapples shouldn't grace our beloved pizzas! Have you never experienced the glory of that juicy fruit melting on top of mozzarella before being warmed by the oven heat? It's like receiving a direct message from God himself: "I love you so much that I will bless you with this culinary wonder." The smell of fresh basil combined with the juice of freshly picked pines makes for an unforgettable experience; why would we limit ourselves to only dried herbs and processed meat? When you add all three together on a homemade crust made with care, there's no reason to resist. Trolling people online and ruining others joy should be against the law; until then, enjoy your sad excuse for a pizza while I revel in mine.”

  

From there the troll will either ignore the conversation, laughing at the outrage he’s caused or will continue arguing and harassing.

  

Other forms of trolls include the spammer (someone who joins just to spam the community), the bully (someone who joins to harass one or more people in the community), and the reporter (someone who joins to report things that clearly don’t need to be reported, wasting the time of the moderators).

  

So how do we avoid this when creating Humein? We must foster the normalcy of NOT. ENGAGING. THE. TROLLS. You see, the only thing trolls crave is attention. By not giving them any attention, they will stop and maybe even reform themselves. Having a pinned document explaining what trolls are, how to make them stop (ignoring them), and fostering a culture of ignoring trolls will all greatly help alleviate this. Troll posts could also be moderated but this document is about how to avoid moderation.

  

Overall though, in most online communities trolls are few and far between

  

### The Passionist <sub>(yes I invented a word be quiet)</sub>

  

The passionist is a normal person (normie) who gets way to passionate, angry, or upset about something and can’t see other’s viewpoints. Typically, a passionist is understandably really upset by something and therefore doesn’t see other viewpoints. A passionist differs from a troll in that they don’t have malicious intentions and are instead just upset about something. A passionist’s mindset rarely changes, they don’t like people with differing viewpoints, and they let their emotions control their words. A passionist is actually just a normal person who gets triggered by certain things (some passionists have many triggers). You and I are actually passionists. Here’s what a typical passionist interaction might look like:

  

Normie: Pineapple on pizza is amazing. Passionist: BS. And anyone who likes it is insane.

  

Seems a lot like a troll doesn’t it? However, most of the time when a passionist is called out they change their behavior and apologize. The difference lies in this. A passionist can realize when they’re acting like a troll.

  

How do we keep ourselves from becoming insufferable passionists? I think it comes down to two things: a) Think three times before you post! Does this need to be said? Will it hurt anyone? Is it constructive criticism? Does this person have a point? Can two reasonable people disagree? Do I see why they believe this? Think through questions like this three times before you post that paragraph explaining why they’re wrong. b) If you see a passionist in the wild, call them out. Be kind and just say something like this: “You’re being really passionate about this and it’s a little harmful.” 98% times the passionist will realize and correct their mistakes.

  

If you get called out for being a passionist accept the feedback and apologize. Everyone makes mistakes, so correct yours. We are all a passionist at some point.

  

### The Normie

  

This is you 90% of people 90% of the time. All normies are unique, with unique opinions. All normies share their opinions but rarely act like a passionist over them. Normies all make mistakes and sometimes act like a passionist (or even one of the less known archetypes like the Low Effort Poster, The Flooder, The Lurker, etc). As a normie it is important for you to ignore+report trolls, kindly talk to passionists, report offensive content, and correct your own mistakes. A Normies shouldn’t be afraid to post for fear of being called out though. As long as it thinks three times before you post you’re done nothing wrong. We’re here to have fun.

 
 ## What to do with this?
Okay, so what do we do with this? We know where issues will arise but how do we prevent it?
 Easy, just be aware of it. By knowing who's a troll, who is overly passionate about something, and knowing who yourself is most issues will be resolved on their own. Knowing who you're talking to (a real person that may be acting like a passionist or troll) taking feedback, and thinking three times before you post is all you need. Be aware of it and minimize the harmful parts. Call those out who need called out and ignore those who won't listen. Report trolls and out of controll passionists.

  **TL;DR: To foster a community that needs minimal moderation the troll must be ignored, the passionist redirected and the normie aware.**


<br><br>

**Bonus groups:**

  

The Low Effort Poster (LEP): Someone who wants to join the discussion that doesn’t have anything meaningful to say. Typically they reply with copypastas, low effort jokes, and “this”. The LEPs are best handled by giving them no attention and perhaps a gentle reminder that it’s not helpful. Do not insult the LEP as it only hurts a person.

  

The Flooder: The flooder is someone who posts a ton of content relating to one thing in a short time. They differ from a spam troll in that they have no ill will and are just posting about their interests. There is nothing inherently wrong with them but if you feel yourself acting like a flooder perhaps it would be best to post your meme collection more slowly and spread out. :)

  

The Lurker: The lurker probably just read this, but will never post so not too much applies to them. They are always watching.

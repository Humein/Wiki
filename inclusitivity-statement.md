# Inclusivity Statement

Welcome to our self-governed community, we're striving to create a self governing, safe, and diverse online community that fosters discussion.

This document will explain many of our guiding principals for our community.

<br>

Our community values autonomy, solidarity, and the dismantling of hierarchical structures. We recognise that diversity is a vital source of strength, and we embrace participants from all cultures, races, ethnicities, genders, sexual orientations, abilities, ages, socioeconomic backgrounds.
In our community, we uphold the following principles to foster inclusivity:

**Respect and Kindness:** We promote a culture of respect and kindness, treating all community members with empathy and consideration. We encourage dialogue that is constructive, supportive, and compassionate.

**Participatory Democracy:** We believe in the power of direct participation and decision-making by all community members. We encourage open discussions, consensus-building, and active involvement in shaping the policies, rules, and direction of our community.

**Non-Discrimination:** We oppose discrimination, harassment, and oppression. Our community has zero tolerance for hate speech, racism, sexism, homophobia, transphobia, ableism, or any form of marginalisation. We strive to create a safe environment where everyone can freely express themselves.

**Collaboration and Mutual Aid:** We embrace the principles of collaboration and mutual aid. Our community encourages members to support each other, share resources, and uplift marginalised voices. We believe that by working together, we can build a more equitable and just society.

**Education and Critical Thinking:** We promote lifelong learning, critical thinking, and the sharing of knowledge. We encourage educational initiatives, skill-sharing workshops, and the exploration of diverse perspectives. We value intellectual curiosity and the pursuit of knowledge as tools for personal and collective growth.

**Accessibility:** We are committed to making our community accessible to all individuals. We strive to remove barriers and accommodate the needs of community members with disabilities, ensuring that everyone can participate fully.

**Equality and Equity:** We are committed to promoting equality and challenging gender-based oppression. We strive for gender justice by encouraging the participation and leadership of people of all genders. We prioritise creating a safe and inclusive environment free from discrimination, sexism, and patriarchal norms.

**Ethnic and Cultural Diversity:** We recognize and celebrate the diversity of our community. We encourage the representation and participation of individuals from different ethnic backgrounds, cultures, and languages. We value cultural exchange, mutual respect, and the preservation of diverse identities.

**Ecological Sustainability:** We are mindful of our impact on the environment and prioritise ecological sustainability. We promote practices that minimize waste, conserve energy, and prioritise sustainable alternatives. We aim to create community infrastructure that is in harmony with nature.

**Conflict Resolution and Restorative Justice:** We prioritise peaceful resolution of conflicts and emphasize restorative justice practices. We encourage dialogue, mediation, and reconciliation to address conflicts within our community. We aim to foster an environment where conflicts are seen as opportunities for growth and understanding.

By joining our community, you become an integral part of our shared commitment to inclusivity and mutual respect. Together, we can create a vibrant and welcoming space where ideas flourish, friendships are formed, and positive change is nurtured.

<br> <br>

*Written by Sam (CurleyWurlies4All), June 2023. Slightly edited, renamed, and repurposed from "Inclusivity Statement" by Henry, August '23.*